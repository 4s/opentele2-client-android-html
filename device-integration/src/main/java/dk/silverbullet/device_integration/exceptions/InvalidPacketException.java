package dk.silverbullet.device_integration.exceptions;

import java.io.IOException;

public class InvalidPacketException extends IOException {

    private static final long serialVersionUID = 8222353424225421574L;

    public InvalidPacketException(String message) {
        super(message);
    }

    public InvalidPacketException() {
        super();
    }

}

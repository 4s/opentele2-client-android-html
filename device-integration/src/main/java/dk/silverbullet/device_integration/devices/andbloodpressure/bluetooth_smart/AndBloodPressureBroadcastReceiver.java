package dk.silverbullet.device_integration.devices.andbloodpressure.bluetooth_smart;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.io.Serializable;

import dk.silverbullet.device_integration.Util;
import dk.silverbullet.device_integration.devices.andbloodpressure.BloodPressureAndPulse;
import dk.silverbullet.device_integration.listeners.DeviceListener;
import dk.silverbullet.device_integration.protocols.bluetooth_smart.GattConstants;
import dk.silverbullet.device_integration.protocols.continua.DeviceInformation;

public class AndBloodPressureBroadcastReceiver extends BroadcastReceiver {

    // --*-- Fields --*--

    private static final String TAG = Util.getTag(AndBloodPressureBroadcastReceiver.class);

    private final DeviceListener<BloodPressureAndPulse> bloodPressureDeviceListener;

    // --*-- Constructors --*--

    public AndBloodPressureBroadcastReceiver(DeviceListener<BloodPressureAndPulse> bloodPressureDeviceListener) {
        this.bloodPressureDeviceListener = bloodPressureDeviceListener;
    }

    // --*-- Methods --*--

    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();

        switch (action) {
            case GattConstants.CONNECTED:
                connected();
                break;

            case GattConstants.DISCONNECTED:
                disconnected();
                break;

            case GattConstants.SERVICES_DISCOVERED:
                servicesDiscovered();
                break;

            case GattConstants.DATA_AVAILABLE:
                dataAvailable(intent);
                break;

            default:
                Log.w(TAG, "Unknown event caught: " + action);
                break;
        }
    }

    private void connected() {
        Log.d(TAG, "CONNECTED");
        bloodPressureDeviceListener.connected();
    }

    private void disconnected() {
        Log.d(TAG, "DISCONNECTED");
        bloodPressureDeviceListener.disconnected();
    }

    private void servicesDiscovered() {
        Log.d(TAG, "SERVICES_DISCOVERED");
    }

    private void dataAvailable(Intent intent) {
        Log.d(TAG, "DATA_AVAILABLE");
        Serializable intentContent = intent.getSerializableExtra(GattConstants.DATA_AVAILABLE);
        // TODO do something else so that we can call
        // bloodPressureDeviceListener.measurementReceived(deviceInformation, bloodPressureAndPulse)
        if (intentContent instanceof BloodPressureAndPulse) {
            BloodPressureAndPulse bloodPressureAndPulse = (BloodPressureAndPulse) intentContent;
            Log.d(TAG, "Received blood pressure and pulse: " + bloodPressureAndPulse.toString());
        } else if (intentContent instanceof DeviceInformation) {
            DeviceInformation deviceInformation = (DeviceInformation) intentContent;
            Log.d(TAG, "Received device information: " + deviceInformation.toString());
        } else {
            Log.d(TAG, "Received something else: " + intentContent.toString());
        }
    }




}

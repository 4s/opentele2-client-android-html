package dk.silverbullet.device_integration.devices.noninsaturation.packet;

public abstract class NoninPacket {

    // --*-- Methods --*--

    int calculateChecksum(Integer[] data) {
        int checksum = 0;
        for (int i : data) {
            checksum += i;
        }
        return checksum % 256;
    }
}

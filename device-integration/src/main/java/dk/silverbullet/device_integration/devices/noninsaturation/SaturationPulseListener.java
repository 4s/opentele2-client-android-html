package dk.silverbullet.device_integration.devices.noninsaturation;

import dk.silverbullet.device_integration.listeners.DeviceListener;
import dk.silverbullet.device_integration.protocols.continua.DeviceInformation;

public interface SaturationPulseListener extends DeviceListener<SaturationAndPulse> {

    void firstTimeOut();

    void finalTimeOut(DeviceInformation deviceInformation, SaturationAndPulse measurement);

}

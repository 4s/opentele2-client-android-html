package dk.silverbullet.device_integration.protocols.continua.protocol;

import android.util.Log;

import java.io.IOException;

import dk.silverbullet.device_integration.Util;
import dk.silverbullet.device_integration.protocols.continua.ContinuaPacketTag;
import dk.silverbullet.device_integration.protocols.continua.PacketParser;
import dk.silverbullet.device_integration.protocols.continua.packet.SystemId;
import dk.silverbullet.device_integration.protocols.continua.packet.input.AbortCommunicationPacket;
import dk.silverbullet.device_integration.protocols.continua.packet.input.AssociationReleaseRequestPacket;
import dk.silverbullet.device_integration.protocols.continua.packet.input.AssociationRequestPacket;
import dk.silverbullet.device_integration.protocols.continua.packet.input.ConfirmedMeasurementDataPacket;
import dk.silverbullet.device_integration.protocols.continua.packet.output.AssociationReleaseResponsePacket;
import dk.silverbullet.device_integration.protocols.continua.packet.output.AssociationResponsePacket;
import dk.silverbullet.device_integration.protocols.continua.packet.output.ConfigurationResponsePacket;
import dk.silverbullet.device_integration.protocols.continua.packet.output.ConfirmedMeasurementResponsePacket;
import dk.silverbullet.device_integration.protocols.continua.packet.output.MedicalDeviceSystemAttributesRequest;

public abstract class ProtocolStateController<MeasurementType,
        ConfirmedMeasurementsType extends ConfirmedMeasurementDataPacket>
        implements PacketParser {

    private static final String TAG = Util.getTag(ProtocolStateController.class);
    private static final int MAX_RESET_COUNT = 5;
    public static final int EXTENDED_CONFIGURATION = 16384;
    public static final int MEDICAL_DEVICE_SYSTEM_ATTRIBUTES_INVOKE_ID = 0x1234; // Arbitrary
    private int resetCount = 0;

    public enum State {
        UNASSOCIATED, ASSOCIATED, MEASUREMENT_RECEIVED, DONE
    }

    protected final ProtocolStateListener<MeasurementType> listener;
    protected State currentState = State.UNASSOCIATED;
    protected SystemId systemId;

    public ProtocolStateController(ProtocolStateListener<MeasurementType> listener) {
        this.listener = listener;
    }

    protected abstract ConfirmedMeasurementsType createConfirmedMeasurementsType(byte[] contents) throws IOException;

    protected abstract void handleConfirmedMeasurement(ConfirmedMeasurementsType confirmedMeasurements);

    protected abstract void handleAssociationReleaseRequest(
            AssociationReleaseRequestPacket associationReleaseRequestPacket);

    public void receive(AssociationRequestPacket associationRequest) {
        Log.d(TAG, "Received AssociationRequestPacket: " + associationRequest);

        if (currentState == State.DONE) {
            Log.w(TAG, "Ignored - in state done!");
        } else if (currentState == State.UNASSOCIATED) {
            systemId = associationRequest.getSystemId();
            try {
                int deviceConfigurationId = associationRequest.getDeviceConfigurationId();
                AssociationResponsePacket associationResponsePacket;
                if (deviceConfigurationId == EXTENDED_CONFIGURATION) {
                    Log.d(TAG, "Association request uses extended configuration");
                    associationResponsePacket = new AssociationResponsePacket(systemId, true);
                } else {
                    Log.d(TAG, "Association request uses standard configuration");
                    associationResponsePacket = new AssociationResponsePacket(systemId, false);
                }
                listener.sendPacket(associationResponsePacket);
                listener.sendPacket(new MedicalDeviceSystemAttributesRequest(MEDICAL_DEVICE_SYSTEM_ATTRIBUTES_INVOKE_ID));

                currentState = State.ASSOCIATED;
            } catch (IOException ex) {
                // Ignore! We're ins state UNASSOCIATED anyway!
            }
        } else {
            Log.e(TAG, "Unexpected protocol state (" + currentState + ") - resetting!");
            resetProtocol();
        }
    }

    public void receive(ConfirmedMeasurementsType confirmedMeasurement) {
        Log.d(TAG, "Received ConfirmedMeasurementData: " + confirmedMeasurement);

        if (currentState == State.DONE) {
            Log.w(TAG, "Ignored - in state done!");
        } else if (currentState == State.ASSOCIATED) {
            if (confirmedMeasurement.getEventType() == ConfirmedMeasurementDataPacket.MDC_NOTI_CONFIG) {
                Log.d(TAG, "ConfirmedMeasurement was a configuration");
                handleConfirmedMeasurement(confirmedMeasurement);
                try {
                    int invokeId = confirmedMeasurement.getInvokeId();
                    listener.sendPacket(new ConfigurationResponsePacket(invokeId));
                } catch (IOException ex) {
                    currentState = State.UNASSOCIATED;
                }
            } else {
                Log.d(TAG, "ConfirmedMeasurement was an actual measurement");
                handleConfirmedMeasurement(confirmedMeasurement);
                try {
                    listener.sendPacket(new ConfirmedMeasurementResponsePacket(confirmedMeasurement.getInvokeId(),
                            confirmedMeasurement.getEventType()));
                } catch (IOException ex) {
                    currentState = State.UNASSOCIATED;
                }
            }
        } else {
            Log.e(TAG, "Unexpected protocol state (" + currentState + ") - resetting!");
            resetProtocol();
        }
    }

    public void receive(AssociationReleaseRequestPacket associationReleaseRequestPacket) {
        Log.d(TAG, "Received AssociationReleaseRequestPacket: " + associationReleaseRequestPacket);

        if (currentState == State.DONE) {
            Log.w(TAG, "Ignored - in state done!");
        } else if (currentState == State.ASSOCIATED || currentState == State.MEASUREMENT_RECEIVED) {
            Log.d(TAG, "State not in [Associated, Measurement received]");

            handleAssociationReleaseRequest(associationReleaseRequestPacket);

            try {
                listener.sendPacket(new AssociationReleaseResponsePacket());
            } catch (IOException e) {
                Log.w(TAG, "Could not send association release response", e);
            }

            currentState = State.DONE;
            listener.finishNow();
        } else {
            Log.e(TAG, "Unexpected protocol state (" + currentState + ") - resetting!");
            resetProtocol();
        }
    }

    public void receive(AbortCommunicationPacket abortCommunication) {
        Log.d(TAG, "Received AbortCommunication: " + abortCommunication);
        resetProtocol();
    }

    @Override
    public void errorReceived(IOException ex) {
        Log.e(TAG, "ProtocolStateController received an error: " + ex.getMessage());
        listener.finish();
        resetProtocol();
    }

    private void resetProtocol() {
        if (currentState == State.DONE) {
            return;
        }

        resetCount++;
        Log.w(TAG, "Protocol reset, count=" + resetCount);
        if (resetCount >= MAX_RESET_COUNT) {
            listener.tooManyRetries();
            currentState = State.DONE;
        } else {
            currentState = State.UNASSOCIATED;
        }
    }

    @Override
    public void reset() {
        Log.d(TAG, "Received reset!");
        currentState = State.UNASSOCIATED;
        resetCount = 0;
    }

    @Override
    public void handle(ContinuaPacketTag tag, byte[] contents) throws IOException {
        switch (tag) {
        case AARQ_APDU:
            receive(new AssociationRequestPacket(contents));
            break;
        case PRST_APDU:
            receive(createConfirmedMeasurementsType(contents));
            break;
        case ABRT_APDU:
            receive(new AbortCommunicationPacket(contents));
            break;
        case RLRQ_APDU:
            receive(new AssociationReleaseRequestPacket(contents));
            break;
        default:
            throw new IllegalStateException("Unknown packet tag: '" + tag + "'");
        }
    }
}

package dk.silverbullet.device_integration.protocols.continua;

import java.util.regex.Pattern;

import dk.silverbullet.device_integration.exceptions.DeviceInitialisationException;

public interface ContinuaDeviceController {

    /**
     * Closes all currently pending communication and terminates the session.
     */
    void close();

    /**
     * @throws DeviceInitialisationException if Bluetooth is not available, not
     * enabled, or if a general Bluetooth error occurs.
     */
    void initiate(Pattern deviceNamePattern, Pattern macAddressPattern) throws DeviceInitialisationException;

}

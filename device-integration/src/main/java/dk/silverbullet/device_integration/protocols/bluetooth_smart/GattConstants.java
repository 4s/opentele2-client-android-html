package dk.silverbullet.device_integration.protocols.bluetooth_smart;

import java.util.UUID;

public class GattConstants {

    // --*-- Constants --*--

    // Broadcast events
    public static final String CONNECTED = "connected";
    public static final String DISCONNECTED = "disconnected";
    public static final String SERVICES_DISCOVERED = "services discovered";
    public static final String DATA_AVAILABLE = "data available";

    // States
    public static final int STATE_DISCONNECTED = 0;
    public static final int STATE_CONNECTING = 1;
    public static final int STATE_CONNECTED = 2;

    // Intent extras
    public static final String EXTRA_DATA = "EXTRA_DATA";
    public static final String EXTRA_DATA_DEVICE_ADDRESS = "EXTRA_DATA_DEVICE_ADDRESS";
    public static final String EXTRA_DATA_SERVICE_ID = "EXTRA_DATA_SERVICE_ID";
    public static final String EXTRA_DATA_CHARACTERISTIC_ID = "EXTRA_DATA_CHARACTERISTIC_ID";

    // Services
    public static final UUID BLOOD_PRESSURE_SERVICE =
            UUID.fromString("00001810-0000-1000-8000-00805f9b34fb");
    public static final UUID WEIGHT_SCALE_SERVICE =
            UUID.fromString("0000181d-0000-1000-8000-00805f9b34fb");

    // Characteristics
    public static final UUID MANUFACTURER_NAME_CHARACTERISTIC =
            UUID.fromString("00002a29-0000-1000-8000-00805f9b34fb");
    public static final UUID CLIENT_CHARACTERISTIC_CONFIG_CHARACTERISTIC =
            UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

    public static final UUID BLOOD_PRESSURE_MEASUREMENT_CHARACTERISTIC =
            UUID.fromString("00002a35-0000-1000-8000-00805f9b34fb");
    public static final UUID WEIGHT_MEASUREMENT_CHARACTERISTIC =
            UUID.fromString("00002a9d-0000-1000-8000-00805f9b34fb");

    // --*-- Constructors --*--

    private GattConstants() {
        throw new RuntimeException("Constants cannot be instantiated.");
    }

}

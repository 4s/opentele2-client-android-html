package dk.silverbullet.device_integration.protocols.continua.packet.output;

public class ConfigurationResponsePacket implements OutputPacket {

    // --*-- Fields --*--

    private final int invokeId;

    // --*-- Constructors --*--

    public ConfigurationResponsePacket(int invokeId) {
        this.invokeId = invokeId;
    }

    // --*-- Methods --*--

    @Override
    public byte[] getContents() {

        OrderedByteWriter writer = new OrderedByteWriter();

        writer.writeShort(0xE700); // APDU CHOICE Type (AareApdu)
        writer.writeShort(0x0016); // CHOICE.length = 22
        writer.writeShort(0x0014); // OCTET STRING.length = 20
        writer.writeShort(invokeId); // invoke-id = 0xXXXX (mirrored from invocation)
        writer.writeShort(0x0201); // CHOICE (Remote Operation Response | Confirmed Event Report)
        writer.writeShort(0x000E); // CHOICE.length = 14
        writer.writeShort(0x0000); // obj-handle = 0
        writer.writeShort(0x0000); // currentTime = 0 #1
        writer.writeShort(0x0000); // currentTime = 0 #2
        writer.writeShort(0x0D1C); // event-type = MDC_NOTI_CONFIG
        writer.writeShort(0x0004); // event-reply-info.length = 4
        writer.writeShort(0x4000); // ConfigReportRsp.config-report-id = 0x4000
        writer.writeShort(0x0000); // ConfigReportRsp.config-result = accepted-config

        return writer.getBytes();
    }

    @Override
    public String toString() {
        return "ConfigurationResponsePacket [invokeId=" + invokeId + "]";
    }
}

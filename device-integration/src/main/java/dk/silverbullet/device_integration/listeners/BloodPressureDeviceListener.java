package dk.silverbullet.device_integration.listeners;

import android.app.Activity;
import android.bluetooth.BluetoothClass;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.regex.Pattern;

import dk.silverbullet.device_integration.Util;
import dk.silverbullet.device_integration.devices.andbloodpressure.BloodPressureAndPulse;
import dk.silverbullet.device_integration.devices.andbloodpressure.bluetooth_smart.AndBloodPressureBroadcastReceiver;
import dk.silverbullet.device_integration.devices.andbloodpressure.continua.AndBloodPressureController;
import dk.silverbullet.device_integration.protocols.continua.ContinuaDeviceController;
import dk.silverbullet.device_integration.protocols.continua.DeviceInformation;
import dk.silverbullet.device_integration.protocols.continua.android.AndroidHdpController;

public class BloodPressureDeviceListener implements DeviceListener<BloodPressureAndPulse> {

    // --*-- Fields --*--

    private static final String TAG = Util.getTag(WeightDeviceListener.class);

    private static final Pattern DEVICE_NAME_PATTERN = Pattern.compile("(" +
            "(A(&|N)D BP UA-767PBT-C.*)|" +
            "(A(&|N)D UA-767PBT-Ci.*)" +
            ")");

    private static final Pattern DEVICE_ADDRESS_PATTERN = Pattern.compile("(" +
            "(00:09:1F:.*)|" +
            "(5C:31:3E:.*)" +
            ")");

    private final Activity activity;
    private final OnEventListener onEventListener;
    private final DefaultDeviceListener defaultDeviceListener;

    // --*-- Constructors --*--

    public BloodPressureDeviceListener(Activity activity, OnEventListener onEventListener) {
        this.activity = activity;
        this.onEventListener = onEventListener;
        this.defaultDeviceListener = new DefaultDeviceListener(activity, onEventListener);
    }

    // --*-- Methods --*--

    @Override
    public void start() {

        AndroidHdpController androidHdpController =
                new AndroidHdpController(activity.getApplicationContext());
        ContinuaDeviceController andBloodPressureDeviceController =
                new AndBloodPressureController(this, androidHdpController);
        AndBloodPressureBroadcastReceiver andBloodPressureBroadcastReceiver =
                new AndBloodPressureBroadcastReceiver(this);

        defaultDeviceListener.connect(andBloodPressureDeviceController,
                andBloodPressureBroadcastReceiver,
                BluetoothClass.Device.HEALTH_BLOOD_PRESSURE,
                DEVICE_ADDRESS_PATTERN, DEVICE_NAME_PATTERN);
    }

    @Override
    public void stop() {
        defaultDeviceListener.stop();
    }

    @Override
    public void connecting() {
        defaultDeviceListener.connecting();
    }

    @Override
    public void connected() {
        defaultDeviceListener.connected();
    }

    @Override
    public void disconnected() {
        defaultDeviceListener.disconnected();
    }

    @Override
    public void permanentProblem() {
        defaultDeviceListener.permanentProblem();
    }

    @Override
    public void temporaryProblem() {
        defaultDeviceListener.temporaryProblem();
    }

    @Override
    public void measurementReceived(final DeviceInformation deviceInformation,
                                    final BloodPressureAndPulse bloodPressureAndPulse) {
        Log.d(TAG, "Measurement received!");

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    Date timestamp = new Date();
                    JSONObject bloodPressureEvent =
                            createBloodPressureEvent(deviceInformation, timestamp, bloodPressureAndPulse);
                    JSONObject pulseEvent =
                            createPulseEvent(deviceInformation, timestamp, bloodPressureAndPulse);
                    onEventListener.onEvent(bloodPressureEvent);
                    onEventListener.onEvent(pulseEvent);
                    disconnected();
                } catch (JSONException e) {
                    Log.e(TAG, "An error occurred while trying to create " +
                            "measurement events: " + e.getMessage());
                    e.printStackTrace();
                    temporaryProblem();
                }
            }
        });
    }

    private JSONObject createBloodPressureEvent(DeviceInformation deviceInformation,
                                                Date timestamp,
                                                BloodPressureAndPulse bloodPressure) throws JSONException {

        JSONObject event = new JSONObject();
        event.put(JSONConstants.TIMESTAMP, timestamp.toString());
        event.put(JSONConstants.DEVICE, defaultDeviceListener.createDeviceObject(deviceInformation));
        event.put(JSONConstants.TYPE, JSONConstants.MEASUREMENT);
        JSONObject measurement = new JSONObject();
        measurement.put(JSONConstants.TYPE, JSONConstants.BLOOD_PRESSURE);
        measurement.put(JSONConstants.UNIT, JSONConstants.MM_HG);
        JSONObject value = new JSONObject();
        value.put(JSONConstants.SYSTOLIC, bloodPressure.getSystolic());
        value.put(JSONConstants.DIASTOLIC, bloodPressure.getDiastolic());
        value.put(JSONConstants.MEAN_ARTERIAL_PRESSURE, bloodPressure.getMeanArterialPressure());
        measurement.put(JSONConstants.VALUE, value);
        event.put(JSONConstants.MEASUREMENT, measurement);
        return event;
    }

    private JSONObject createPulseEvent(DeviceInformation deviceInformation,
                                        Date timestamp,
                                        BloodPressureAndPulse pulse) throws JSONException {

        JSONObject event = new JSONObject();
        event.put(JSONConstants.TIMESTAMP, timestamp.toString());
        event.put(JSONConstants.DEVICE, defaultDeviceListener.createDeviceObject(deviceInformation));
        event.put(JSONConstants.TYPE, JSONConstants.MEASUREMENT);
        JSONObject measurement = new JSONObject();
        measurement.put(JSONConstants.TYPE, JSONConstants.PULSE);
        measurement.put(JSONConstants.UNIT, JSONConstants.BPM);
        measurement.put(JSONConstants.VALUE, pulse.getPulse());
        event.put(JSONConstants.MEASUREMENT, measurement);
        return event;
    }

}
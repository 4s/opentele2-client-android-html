package dk.silverbullet.device_integration.listeners;

import dk.silverbullet.device_integration.protocols.continua.DeviceInformation;

public interface DeviceListener<T> {

    void start();

    void stop();

    void connecting();

    void connected();

    void disconnected();

    void permanentProblem();

    void temporaryProblem();

    void measurementReceived(DeviceInformation deviceInformation, T measurement);

}

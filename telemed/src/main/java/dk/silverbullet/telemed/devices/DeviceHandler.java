package dk.silverbullet.telemed.devices;

import android.app.Activity;
import android.util.Log;

import java.util.HashMap;

import dk.silverbullet.device_integration.listeners.BloodPressureDeviceListener;
import dk.silverbullet.device_integration.listeners.DeviceListener;
import dk.silverbullet.device_integration.listeners.SaturationDeviceListener;
import dk.silverbullet.device_integration.listeners.WeightDeviceListener;
import dk.silverbullet.telemed.Util;
import dk.silverbullet.telemed.webview.nativetohtml5.WebViewMessageDispatcher;

public class DeviceHandler {

    // --*-- Fields --*--

    private static final String TAG = Util.getTag(DeviceHandler.class);

    private static final String WEIGHT = "weight";
    private static final String BLOOD_PRESSURE = "blood pressure";
    private static final String SATURATION = "saturation";

    private final HashMap<String, DeviceListener> deviceListenerMap;
    private final WebViewMessageDispatcher webViewMessageDispatcher;
    private final Activity activity;

    private DeviceListener deviceListener;

    // --*-- Constructors --*--

    public DeviceHandler(Activity activity, WebViewMessageDispatcher webViewMessageDispatcher) {
        this.activity = activity;
        this.webViewMessageDispatcher = webViewMessageDispatcher;
        this.deviceListenerMap = new HashMap<>();
    }

    // --*-- Methods --*--

    public void addDeviceListener(String measurementType) {
        populateDeviceListenerMap();

        if (deviceListenerMap.containsKey(measurementType)) {

            if (deviceListener != null) {
                Log.e(TAG, "A device listener already exists, attempting to close it.");
                removeDeviceListeners();
            }

            deviceListener = deviceListenerMap.get(measurementType);
            deviceListener.start();
        } else {
            Log.e(TAG, "Tried to add device listener for unknown measurement type: " + measurementType);
        }
    }

    public void removeDeviceListeners() {
        if (deviceListener != null) {
            deviceListener.stop();
            deviceListener = null;
        }
    }

    private void populateDeviceListenerMap() {
        deviceListenerMap.put(WEIGHT, new WeightDeviceListener(activity, new OnDeviceEventListener(webViewMessageDispatcher, WEIGHT)));
        deviceListenerMap.put(BLOOD_PRESSURE, new BloodPressureDeviceListener(activity, new OnDeviceEventListener(webViewMessageDispatcher, BLOOD_PRESSURE)));
        deviceListenerMap.put(SATURATION, new SaturationDeviceListener(activity, new OnDeviceEventListener(webViewMessageDispatcher, SATURATION)));
    }

}
package dk.silverbullet.telemed.video;

import com.google.gson.Gson;

import org.json.JSONObject;

public class VideoConference {

    // --*-- Fields --*--

    private String username;
    private String roomKey;
    private String serviceUrl;

    // --*-- Constructors --*--

    public VideoConference(String username, String roomKey, String serviceUrl) {
        this.username = username;
        this.roomKey = roomKey;
        this.serviceUrl = serviceUrl;
    }

    public static VideoConference fromJson(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, VideoConference.class);
    }

    // --*-- Methods --*--

    public String getUsername() {
        return username;
    }

    public String getRoomKey() {
        return roomKey;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }
}

package dk.silverbullet.telemed.video;

import android.app.Fragment;
import android.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import dk.silverbullet.telemed.MainActivity;
import dk.silverbullet.telemed.R;
import dk.silverbullet.telemed.Util;
import dk.silverbullet.telemed.webview.nativetohtml5.Html5Hook;
import dk.silverbullet.telemed.webview.nativetohtml5.NativeEventEmitter;
import dk.silverbullet.telemed.webview.nativetohtml5.WebViewMessageDispatcher;

public class VideoCallbackImpl {

    // --*-- Fields --*--

    private static final String TAG = Util.getTag(VideoCallbackImpl.class);

    private static final String INFO = "info";
    private static final String ERROR = "error";
    private static final String TYPE = "type";
    private static final String STATUS = "status";
    private static final String MESSAGE = "message";
    private static final String TIMESTAMP = "timestamp";

    private static final String CONFERENCE_STARTED = "conference started";
    private static final String CONFERENCE_ENDED = "conference ended";
    private static final String CONFERENCE_ERROR = "conference error";

    private MainActivity activity;
    private final WebViewMessageDispatcher webViewMessageDispatcher;
    private NativeEventEmitter eventEmitter;
    private Html5Hook html5Hook;
    private Fragment videoFragment;

    // --*-- Constructors --*--

    public VideoCallbackImpl(MainActivity activity,
                             WebViewMessageDispatcher webViewMessageDispatcher) {
        this.activity = activity;
        this.webViewMessageDispatcher = webViewMessageDispatcher;
    }

    // --*-- Methods --*--

    public void onConferenceStarted() {
        try {
            Log.i(TAG, "Conference started");
            JSONObject statusEvent = createStatusEvent(INFO, CONFERENCE_STARTED);
            emitEvent(statusEvent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        showVideoFragment();
    }

    private void showVideoFragment() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
              //Start video conference
            }
        });
    }

    private void hideVideoFragment() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //Stop video conference
            }
        });
    }

    private void splitScreen(boolean splitScreen) {
        final int videoWeight = splitScreen ? 1 : 0;
        final int webViewWeight = splitScreen ? 1 : 2;
        final int height = 0;
        final int width = LinearLayout.LayoutParams.MATCH_PARENT;

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                FrameLayout videoFrame = (FrameLayout) activity.findViewById(R.id.videoFrame);
                LinearLayout.LayoutParams videoFrameParams = new LinearLayout.LayoutParams(width, height, videoWeight);
                videoFrame.setLayoutParams(videoFrameParams);

                FrameLayout webViewFrame = (FrameLayout) activity.findViewById(R.id.webViewFrame);
                LinearLayout.LayoutParams webViewFrameParams = new LinearLayout.LayoutParams(width, height, webViewWeight);
                webViewFrameParams.gravity = Gravity.BOTTOM;
                webViewFrame.setLayoutParams(webViewFrameParams);
            }
        });
    }

    public void onConferenceEnded() {
        try {
            Log.i(TAG, "Conference ended");
            hideVideoFragment();
            activity.setVideoInProgress(false);
            JSONObject statusEvent = createStatusEvent(INFO, CONFERENCE_ENDED);
            emitEvent(statusEvent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onError() {
        try {
            Log.i(TAG, "Conference error");
            hideVideoFragment();
            activity.setVideoInProgress(false);
            JSONObject errorEvent = createStatusEvent(ERROR, CONFERENCE_ERROR);
            emitEvent(errorEvent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void emitEvent(final JSONObject event) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    JSONObject message = new JSONObject();
                    message.put("messageType", "videoConferenceResponse");
                    message.put("event", event);
                    webViewMessageDispatcher.send(message);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private JSONObject createStatusEvent(String type, String message)
            throws JSONException {

        String timestamp = new Date().toString();
        JSONObject status = new JSONObject();
        status.put(TYPE, type);
        status.put(MESSAGE, message);

        JSONObject event = new JSONObject();
        event.put(TIMESTAMP, timestamp);
        event.put(TYPE, STATUS);
        event.put(STATUS, status);

        return event;
    }

}
package dk.silverbullet.telemed.webview.html5tonative;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import dk.silverbullet.telemed.MainActivity;
import dk.silverbullet.telemed.Util;
import dk.silverbullet.telemed.webview.nativetohtml5.WebViewMessageDispatcher;

public class DeviceInformationRequestHandler implements MessageHandler {

    private final MainActivity mainActivity;
    private final WebViewMessageDispatcher webViewMessageDispatcher;

    public DeviceInformationRequestHandler(MainActivity mainActivity, WebViewMessageDispatcher webViewMessageDispatcher) {
        this.mainActivity = mainActivity;
        this.webViewMessageDispatcher = webViewMessageDispatcher;
    }

    @Override
    public String getMessageTypeHandled() {
        return "deviceInformationRequest";
    }

    @Override
    public void consume(JSONObject message) throws JSONException {

        JSONObject response = new JSONObject(Util.getDeviceInformation(mainActivity));
        response.put("messageType", "deviceInformationResponse");

        webViewMessageDispatcher.send(response);
    }
}

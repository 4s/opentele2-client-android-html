package dk.silverbullet.telemed.webview.html5tonative;

import org.json.JSONException;
import org.json.JSONObject;

import dk.silverbullet.telemed.MainActivity;

public class StopNotificationSoundRequestHandler implements MessageHandler {

    private final MainActivity mainActivity;

    public StopNotificationSoundRequestHandler(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public String getMessageTypeHandled() {
        return "stopNotificationSoundRequest";
    }

    @Override
    public void consume(JSONObject message) throws JSONException {
        mainActivity.stopNotificationSound();
    }
}

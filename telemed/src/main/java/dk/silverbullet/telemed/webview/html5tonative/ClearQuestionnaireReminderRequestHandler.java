package dk.silverbullet.telemed.webview.html5tonative;

import org.json.JSONException;
import org.json.JSONObject;

import dk.silverbullet.telemed.MainActivity;
import dk.silverbullet.telemed.reminder.ReminderService;

public class ClearQuestionnaireReminderRequestHandler implements MessageHandler {

    private final MainActivity mainActivity;

    public ClearQuestionnaireReminderRequestHandler(MainActivity mainActivity) {

        this.mainActivity = mainActivity;
    }

    @Override
    public String getMessageTypeHandled() {
        return "clearQuestionnaireReminderRequest";
    }

    @Override
    public void consume(JSONObject message) throws JSONException {
        ReminderService.clearRemindersForQuestionnaire(
                mainActivity.getApplicationContext(), message.getString("questionnaireName"));
    }
}

package dk.silverbullet.telemed.webview;

import android.content.Context;
import android.os.AsyncTask;
import android.text.InputType;
import android.util.AttributeSet;
import android.util.Log;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.webkit.WebView;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import dk.silverbullet.telemed.Constants;
import dk.silverbullet.telemed.Util;
import dk.silverbullet.telemed.webview.nativetohtml5.Html5Hook;
import dk.silverbullet.telemed.webview.nativetohtml5.NativeEventEmitter;

/**
 * Loads a URL but displays a custom OpenTele error page instead of the
 * browser's 404/500 error page.
 */
public class Html5WebView extends WebView implements NativeEventEmitter {

    // --*-- Fields --*--

    private static final String TAG = Util.getTag(Html5WebView.class);

    public static final String LOCAL_FILE_PATH = "file:///";
    public static final String JAVASCRIPT_PREFIX = "javascript:";

    // --*-- Constructors --*--

    public Html5WebView(Context context) {
        super(context);
    }

    public Html5WebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Html5WebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void loadUrl(final String url) {
        new AsyncTask<String, Void, Void>() {
            @Override
            protected void onPostExecute(Void isAvailable) {
                Log.d(TAG, "Contacted url:" + url);
                Html5WebView.super.loadUrl(url);
            }

            protected Void doInBackground(String... params) {
                return null;
            }
        }.execute(url);
    }

    @Override
    public void emitEvent(JSONObject event, Html5Hook html5Hook) {
        Log.d(TAG, event.toString());
        loadUrl(wrapEvent(event, html5Hook));
    }

    private String wrapEvent(JSONObject event, Html5Hook html5Hook) {
        String elementId = html5Hook.getElementId();
        String modelName = html5Hook.getModelName();
        String callbackName = html5Hook.getCallbackName();
        String javaScriptCode = "javascript:" +
                    "var scope = angular.element('#" + elementId + "').scope(); " +
                    "scope.$apply(function() {" +
                        "  scope['" + modelName + "']['" + callbackName + "'](" + event.toString() + ");" +
                    "});";
        Log.d(TAG, javaScriptCode);
        return javaScriptCode;
    }

    @Override
    public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
        InputConnection connection = super.onCreateInputConnection(outAttrs);
        if ((outAttrs.inputType & InputType.TYPE_CLASS_NUMBER) == InputType.TYPE_CLASS_NUMBER) {
            outAttrs.inputType |= InputType.TYPE_NUMBER_FLAG_DECIMAL;
        }
        return connection;
    }
}

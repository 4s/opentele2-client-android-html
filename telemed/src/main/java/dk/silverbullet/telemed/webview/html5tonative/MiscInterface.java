package dk.silverbullet.telemed.webview.html5tonative;

import android.content.Intent;
import android.net.Uri;
import android.webkit.JavascriptInterface;

import com.google.gson.Gson;

import dk.silverbullet.telemed.MainActivity;
import dk.silverbullet.telemed.Util;
import dk.silverbullet.telemed.webview.html5tonative.JavaScriptInterface;

@SuppressWarnings("UnusedDeclaration")
public class MiscInterface implements JavaScriptInterface {

    // --*-- Fields --*--

    private MainActivity mainActivity;

    // --*-- Constructors --*--

    /**
     * @param mainActivity - the main activity.
     */
    public MiscInterface(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    // --*-- Methods --*--

    @Override
    public String getName() {
        return "Misc";
    }

    /**
     * @return a stringified representation of the JSON object describing the device information.
     */
    @JavascriptInterface
    public String getDeviceInformation() {
        Gson gson = new Gson();
        return gson.toJson(Util.getDeviceInformation(mainActivity));
    }

    @JavascriptInterface
    public String getPatientPrivacyPolicy() {
        return Util.getPatientPrivacyPolicy(mainActivity);
    }

    @JavascriptInterface
    public void openUrl(String url) {
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        mainActivity.startActivity(i);
    }
}

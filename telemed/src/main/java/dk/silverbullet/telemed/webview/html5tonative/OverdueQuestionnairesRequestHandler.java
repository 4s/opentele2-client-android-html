package dk.silverbullet.telemed.webview.html5tonative;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import dk.silverbullet.telemed.reminder.ReminderService;
import dk.silverbullet.telemed.webview.nativetohtml5.WebViewMessageDispatcher;

public class OverdueQuestionnairesRequestHandler implements MessageHandler{

    private final WebViewMessageDispatcher webViewMessageDispatcher;

    public OverdueQuestionnairesRequestHandler(WebViewMessageDispatcher webViewMessageDispatcher) {

        this.webViewMessageDispatcher = webViewMessageDispatcher;
    }

    @Override
    public String getMessageTypeHandled() {
        return "overdueQuestionnairesRequest";
    }

    @Override
    public void consume(JSONObject message) throws JSONException {
        List<String> questionnaireNames = ReminderService.getQuestionnairesToHighlight();

        JSONObject response = new JSONObject();
        response.put("messageType", "overdueQuestionnairesResponse");
        response.put("questionnaireNames", new JSONArray(questionnaireNames));

        webViewMessageDispatcher.send(response);
    }
}

package dk.silverbullet.telemed.webview.html5tonative;

import org.json.JSONException;
import org.json.JSONObject;

import dk.silverbullet.telemed.MainActivity;
import dk.silverbullet.telemed.reminder.ReminderService;

public class SetupRemindersRequestHandler implements MessageHandler {

    private final MainActivity mainActivity;

    public SetupRemindersRequestHandler(MainActivity mainActivity) {

        this.mainActivity = mainActivity;
    }

    @Override
    public String getMessageTypeHandled() {
        return "setupRemindersRequest";
    }

    @Override
    public void consume(JSONObject message) throws JSONException {
        ReminderService.setRemindersTo(mainActivity.getApplicationContext(), message.getJSONArray("reminders").toString());
    }
}
